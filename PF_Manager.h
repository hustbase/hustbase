#ifndef PF_MANAGER_H_H
#define PF_MANAGER_H_H

#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>

#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <malloc.h>
#include <windows.h>

#include "RC.h"

#define PF_PAGE_SIZE ((1<<12)-4)	// 一个页面大小为4k
#define PF_FILESUBHDR_SIZE (sizeof(PF_FileSubHeader))
#define PF_BUFFER_SIZE 50			//页面缓冲区的大小
#define PF_PAGESIZE (1<<12)
typedef unsigned int PageNum;

typedef struct{
	PageNum pageNum;
	char pData[PF_PAGE_SIZE];
	//Page的结构需要着重理解 对于0号页面pData=PF_FileSubHeader+pBitmap，对于其他页面pData存实际data 详见openfile函数@1
}Page;  //页面 = 页号+data区

typedef struct{
	PageNum pageCount;  //该文件可存多少页
	int nAllocatedPages;//该文件已存多少页
}PF_FileSubHeader;// 页面文件 头

typedef struct{
	bool bDirty;
	unsigned int pinCount;
	clock_t  accTime;
	char *fileName;
	int fileDesc;
	Page page;
}Frame;//缓冲页面 Frame=page+控制信息

typedef struct{
	bool bopen;
	char *fileName;
	int fileDesc;
	Frame *pHdrFrame;
	//以下三行变量定义信息其实已经包含在pHdrFrame中，只是为了访问方便，又做了一份拷贝，具体赋值详见openFile函数@1
	Page *pHdrPage;
	char *pBitmap;
	PF_FileSubHeader *pFileSubHeader;
}PF_FileHandle;//页面文件 句柄

typedef struct{
	int nReads;
	int nWrites;
	Frame frame[PF_BUFFER_SIZE];
	bool allocated[PF_BUFFER_SIZE];
}BF_Manager;//整个缓冲区及其控制信息

typedef struct{
	bool bOpen;
	Frame *pFrame;
}PF_PageHandle;//页面 句柄

const RC CreateFile(const char *fileName);
const RC openFile(char *fileName,PF_FileHandle *fileHandle);
const RC CloseFile(PF_FileHandle *fileHandle);

const RC GetThisPage(PF_FileHandle *fileHandle,PageNum pageNum,PF_PageHandle *pageHandle);
const RC AllocatePage(PF_FileHandle *fileHandle,PF_PageHandle *pageHandle);
const RC GetPageNum(PF_PageHandle *pageHandle,PageNum *pageNum);

const RC GetData(PF_PageHandle *pageHandle,char **pData);
const RC DisposePage(PF_FileHandle *fileHandle,PageNum pageNum);

const RC MarkDirty(PF_PageHandle *pageHandle);

const RC UnpinPage(PF_PageHandle *pageHandle);

#endif