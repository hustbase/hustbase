#include "stdafx.h"
#include "PF_Manager.h"

BF_Manager bf_manager;

const RC AllocateBlock(Frame **buf);
const RC DisposeBlock(Frame *buf);
const RC ForceAllPages(PF_FileHandle *fileHandle);

void inti()
{
	int i;
	//init 整个缓冲区（50*4k）
	for(i=0;i<PF_BUFFER_SIZE;i++){
		bf_manager.allocated[i]=false;
		bf_manager.frame[i].pinCount=0;
	}
}

//根据文件句柄和页号获取指定页面到缓冲区，返回页面句柄指针。
const RC CreateFile(const char *fileName)
{
	int fd;
	char *bitmap;
	PF_FileSubHeader *fileSubHeader;
	fd=_open(fileName,_O_RDWR|_O_CREAT|_O_EXCL|_O_BINARY,_S_IREAD|_S_IWRITE);
	if(fd<0)
		return PF_EXIST;
	_close(fd);
	fd=open(fileName,_O_RDWR); //创建并打开一个文件流
	Page page;
	memset(&page,0,PF_PAGESIZE);
	bitmap=page.pData+(int)PF_FILESUBHDR_SIZE;     
	fileSubHeader=(PF_FileSubHeader *)page.pData; //0号页面写页面控制信息，不存储真实data
	fileSubHeader->nAllocatedPages=1;  //设置已用页面数
	bitmap[0]|=0x01;						//------------------>>bitmap没看明白是干什么的？？？？
	if(_lseek(fd,0,SEEK_SET)==-1)    
		return PF_FILEERR;
	if(_write(fd,(char *)&page,sizeof(Page))!=sizeof(Page)){ //写入0号页面
		_close(fd);
		return PF_FILEERR;
	}
	if(_close(fd)<0)    //关闭文件流
		return PF_FILEERR;
	return SUCCESS;
}

PF_FileHandle * getPF_FileHandle(void )
{
	PF_FileHandle *p=(PF_FileHandle *)malloc(sizeof( PF_FileHandle));
	p->bopen=false;
	return p;
}                        

const RC openFile(char *fileName,PF_FileHandle *fileHandle)
{
	//对PF_FileHandle结构各个属性的设置
	int fd;
	PF_FileHandle *pfilehandle=fileHandle;
	RC tmp;
	if((fd=_open(fileName,O_RDWR|_O_BINARY))<0)
		return PF_FILEERR;
	pfilehandle->bopen=true;
	pfilehandle->fileName=fileName;
	pfilehandle->fileDesc=fd;
	//默认打开文件就读第一个Page进来---------->0号页面存控制信息
	if((tmp=AllocateBlock(&pfilehandle->pHdrFrame))!=SUCCESS){      //为pHdrFrame分配block，句柄里只存指针，所以在堆上开辟空间
		_close(fd);
		return tmp;
	}
	//为pHdrFrame设置相应属性
	pfilehandle->pHdrFrame->bDirty=false;
	pfilehandle->pHdrFrame->pinCount=1;
	pfilehandle->pHdrFrame->accTime=clock();
	pfilehandle->pHdrFrame->fileDesc=fd;
	pfilehandle->pHdrFrame->fileName=fileName;
	if(_lseek(fd,0,SEEK_SET)==-1){
		DisposeBlock(pfilehandle->pHdrFrame);   //将之前的AllocateBlock操作析构，防止文件操作错误（文件操作错误是必须考虑的危险操作）而引起内存泄漏
		_close(fd);
		return PF_FILEERR;
	}
	if(_read(fd,&(pfilehandle->pHdrFrame->page),sizeof(Page))!=sizeof(Page)){ //读一个Page到pfilehandle->pHdrFrame->page 即每次调入一个Page到缓冲区
		DisposeBlock(pfilehandle->pHdrFrame);
		_close(fd);
		return PF_FILEERR;
	}
		//pHdrFrame 已包含如下三行信息@1
	pfilehandle->pHdrPage=&(pfilehandle->pHdrFrame->page);  
	pfilehandle->pBitmap=pfilehandle->pHdrPage->pData+PF_FILESUBHDR_SIZE;
	/*
		//???pBitmap是pfilehandle（与文件一一对应）中一片连续空间（从AllocatePage函数对pBitmap的访问可以看出），它的地址为什么要给到第0号页面pData+PF_FILESUBHDR_SIZE的地方
		---->因为0号页面是存控制信息的，详见Page数据结构说明@1
	*/
	pfilehandle->pFileSubHeader=(PF_FileSubHeader *)pfilehandle->pHdrPage->pData;
	fileHandle=pfilehandle;
	return SUCCESS;
}

const RC CloseFile(PF_FileHandle *fileHandle)
{
	RC tmp;
	fileHandle->pHdrFrame->pinCount--;
	if((tmp=ForceAllPages(fileHandle))!=SUCCESS) //处理脏数据直接强制关掉所有页面，详见定义处
		return tmp;
	if(_close(fileHandle->fileDesc)<0)  //关闭文件流
		return PF_FILEERR;
	return SUCCESS;
}

const RC AllocateBlock(Frame **buffer)
{
	int i,min,offset;
	bool flag;
	clock_t mintime;
	for(i=0;i<PF_BUFFER_SIZE;i++)
		if(bf_manager.allocated[i]==false){
			bf_manager.allocated[i]=true;
			*buffer=bf_manager.frame+i;
			return SUCCESS;
		}
	flag=false;
	for(i=0;i<PF_BUFFER_SIZE;i++){
		if(bf_manager.frame[i].pinCount!=0)
			continue;
		if(flag==false){
			flag=true;
			min=i;
			mintime=bf_manager.frame[i].accTime;
		}
		if(bf_manager.frame[i].accTime<mintime){
			min=i;
			mintime=bf_manager.frame[i].accTime;
		}
	}
	if(flag==false)
		return PF_NOBUF;
	if(bf_manager.frame[min].bDirty==true){
		offset=(bf_manager.frame[min].page.pageNum)*sizeof(Page);
		if(_lseek(bf_manager.frame[min].fileDesc,offset,SEEK_SET)==offset-1)
			return PF_FILEERR;
		if(_write(bf_manager.frame[min].fileDesc,&(bf_manager.frame[min].page),sizeof(Page))!=sizeof(Page))
			return PF_FILEERR;
	}
	*buffer=bf_manager.frame+min;
	return SUCCESS;
}


//AllocateBlock的逆操作
const RC DisposeBlock(Frame *buf)
{
	if(buf->pinCount!=0)
		return PF_PAGEPINNED;
	if(buf->bDirty==true){
		if(_lseek(buf->fileDesc,buf->page.pageNum*sizeof(Page),SEEK_SET)<0)
			return PF_FILEERR;
		if(_write(buf->fileDesc,&(buf->page),sizeof(Page))!=sizeof(Page))
			return PF_FILEERR;
	}
	buf->bDirty=false;
	bf_manager.allocated[buf-bf_manager.frame]=false;
	return SUCCESS;
}

PF_PageHandle* getPF_PageHandle()
{
	PF_PageHandle *p=(PF_PageHandle *)malloc(sizeof(PF_PageHandle));
	p->bOpen=false;
	return p;
}

const RC GetThisPage(PF_FileHandle *fileHandle,PageNum pageNum,PF_PageHandle *pageHandle)
{
	int i,nread,offset;
	RC tmp;
	PF_PageHandle *pPageHandle=pageHandle;
	if(pageNum>fileHandle->pFileSubHeader->pageCount)
		return PF_INVALIDPAGENUM;
	if((fileHandle->pBitmap[pageNum/8]&(1<<(pageNum%8)))==0)
		return PF_INVALIDPAGENUM;
	pPageHandle->bOpen=true;
	for(i=0;i<PF_BUFFER_SIZE;i++){
		if(bf_manager.allocated[i]==false)
			continue;
		if(strcmp(bf_manager.frame[i].fileName,fileHandle->fileName)!=0)
			continue;
		if(bf_manager.frame[i].page.pageNum==pageNum){
			pPageHandle->pFrame=bf_manager.frame+i;
			pPageHandle->pFrame->pinCount++;
			pPageHandle->pFrame->accTime=clock();
			return SUCCESS;
		}
	}
	if((tmp=AllocateBlock(&(pPageHandle->pFrame)))!=SUCCESS){
		return tmp;
	}
	pPageHandle->pFrame->bDirty=false;
	pPageHandle->pFrame->fileDesc=fileHandle->fileDesc;
	pPageHandle->pFrame->fileName=fileHandle->fileName;
	pPageHandle->pFrame->pinCount=1;
	pPageHandle->pFrame->accTime=clock();
	offset=pageNum*sizeof(Page);
	if(_lseek(fileHandle->fileDesc,offset,SEEK_SET)==offset-1){
		bf_manager.allocated[pPageHandle->pFrame-bf_manager.frame]=false;
		return PF_FILEERR;
	}
	if((nread=read(fileHandle->fileDesc,&(pPageHandle->pFrame->page),sizeof(Page)))!=sizeof(Page)){
		bf_manager.allocated[pPageHandle->pFrame-bf_manager.frame]=false;
		return PF_FILEERR;
	}
	return SUCCESS;
}

//在指定文件中分配一个新的页面，并将其放入缓冲区，返回页面句柄指针。
//分配页面时，如果文件中有空闲页，就直接分配一个空闲页；如果文件中没有空闲页，则扩展文件规模来增加新的空闲页。
const RC AllocatePage(PF_FileHandle *fileHandle,PF_PageHandle *pageHandle)
{
	PF_PageHandle *pPageHandle=pageHandle;
	RC tmp;
	int i,byte,bit;
	fileHandle->pHdrFrame->bDirty=true;
	if((fileHandle->pFileSubHeader->nAllocatedPages)<=(fileHandle->pFileSubHeader->pageCount)){
		//有空闲页 --- 直接分配一个空闲页
		for(i=0;i<=fileHandle->pFileSubHeader->pageCount;i++){
			byte=i/8;
			bit=i%8;
			if(((fileHandle->pBitmap[byte])&(1<<bit))==0){
				(fileHandle->pFileSubHeader->nAllocatedPages)++;
				fileHandle->pBitmap[byte]|=(1<<bit);
				break;//找到第一个pBitmap为0的页面 
			}
		}
		if(i<=fileHandle->pFileSubHeader->pageCount)
			return GetThisPage(fileHandle,i,pageHandle);
		
	}
	fileHandle->pFileSubHeader->nAllocatedPages++;
	fileHandle->pFileSubHeader->pageCount++;
	byte=fileHandle->pFileSubHeader->pageCount/8;
	bit=fileHandle->pFileSubHeader->pageCount%8;
	fileHandle->pBitmap[byte]|=(1<<bit);
	if((tmp=AllocateBlock(&(pPageHandle->pFrame)))!=SUCCESS){ //为新分配的空闲页重新分配一个block----->现在还是在堆里分配
		return tmp;
	}
	pPageHandle->pFrame->bDirty=false;
	pPageHandle->pFrame->fileDesc=fileHandle->fileDesc;
	pPageHandle->pFrame->fileName=fileHandle->fileName;
	pPageHandle->pFrame->pinCount=1;
	pPageHandle->pFrame->accTime=clock();
	memset(&(pPageHandle->pFrame->page),0,sizeof(Page));
	pPageHandle->pFrame->page.pageNum=fileHandle->pFileSubHeader->pageCount;
	//以下将新分配的页面写回文件（磁盘）------>增加文件规模
	if(_lseek(fileHandle->fileDesc,0,SEEK_END)==-1){
		bf_manager.allocated[pPageHandle->pFrame-bf_manager.frame]=false;  //frame类型作为数组下标？？？？
		return PF_FILEERR;
	}
	if(_write(fileHandle->fileDesc,&(pPageHandle->pFrame->page),sizeof(Page))!=sizeof(Page)){ //将该页面数据写入文件
		bf_manager.allocated[pPageHandle->pFrame-bf_manager.frame]=false;
		return PF_FILEERR;
	}
	
	return SUCCESS;
}

const RC GetPageNum(PF_PageHandle *pageHandle,PageNum *pageNum)
{
	if(pageHandle->bOpen==false)
		return PF_PHCLOSED;
	*pageNum=pageHandle->pFrame->page.pageNum;
	return SUCCESS;
}

const RC GetData(PF_PageHandle *pageHandle,char **pData)
{
	if(pageHandle->bOpen==false)
		return PF_PHCLOSED;
	*pData=pageHandle->pFrame->page.pData;
	return SUCCESS;
}

//丢弃文件中编号为pageNum的页面，将其变为空闲页
const RC DisposePage(PF_FileHandle *fileHandle,PageNum pageNum)
{
	int i;
	char tmp;
	if(pageNum>fileHandle->pFileSubHeader->pageCount)
		return PF_INVALIDPAGENUM;
	if(((fileHandle->pBitmap[pageNum/8])&(1<<(pageNum%8)))==0)
		return PF_INVALIDPAGENUM;
	fileHandle->pHdrFrame->bDirty=true;
	fileHandle->pFileSubHeader->nAllocatedPages--;
	tmp=1<<(pageNum%8);
	fileHandle->pBitmap[pageNum/8]&=~tmp;
	for(i=0;i<PF_BUFFER_SIZE;i++){
		if(bf_manager.allocated[i]==false)
			continue;
		if(strcmp(bf_manager.frame[i].fileName,fileHandle->fileName)!=0)
			continue;
		if(bf_manager.frame[i].page.pageNum==pageNum){
			if(bf_manager.frame[i].pinCount!=0)
				return PF_PAGEPINNED;
			bf_manager.allocated[i]=false;
			return SUCCESS;
		}
	}
	return SUCCESS;
}

//标记指定页面为“脏”页。如果修改了页面的内容，则应调用此函数，以便该页面被淘汰出缓冲区时系统将新的页面数据写入磁盘文件。
const RC MarkDirty(PF_PageHandle *pageHandle)
{
	pageHandle->pFrame->bDirty=true;
	return SUCCESS;
}

//此函数用于解除pageHandle对应页面的驻留缓冲区限制。
//在调用GetThisPage或AllocatePage函数将一个页面读入缓冲区后，该页面被设置为驻留缓冲区状态，以防止其在处理过程中被置换出去，
//因此在该页面使用完之后应调用此函数解除该限制，使得该页面此后可以正常地被淘汰出缓冲区。
const RC UnpinPage(PF_PageHandle *pageHandle)
{
	pageHandle->pFrame->pinCount--;
	return SUCCESS;
}

//该函数含有多线程处理对pinCount的判断，但未找到该函数调用处------？
const RC ForcePage(PF_FileHandle *fileHandle,PageNum pageNum)
{
	int i;
	//for循环 处理整个缓冲区脏数据
	for(i=0;i<PF_BUFFER_SIZE;i++){
		if(bf_manager.allocated[i]==false)  //未使用该页
			continue;
		if(strcmp(bf_manager.frame[i].fileName,fileHandle->fileName)!=0) //该页未脏
			continue;
		if(bf_manager.frame[i].page.pageNum==pageNum){   //找到指定页面
			if(bf_manager.frame[i].pinCount!=0)          //仍有进程访问该缓冲区，不做脏数据处理并返回相应信号
				return PF_PAGEPINNED;
			if(bf_manager.frame[i].bDirty==true){				//将脏数据写回磁盘
				if(_lseek(fileHandle->fileDesc,pageNum*PF_PAGESIZE,SEEK_SET)<0)
					return PF_FILEERR;
				if(_write(fileHandle->fileDesc,&(bf_manager.frame[i].page),PF_PAGESIZE)<0)
					return PF_FILEERR;
			}
			bf_manager.allocated[i]=false;     //无线程访问该页且进行脏数据处理之后，将标志位置位false
		}
	}
	return SUCCESS;
}

const RC ForceAllPages(PF_FileHandle *fileHandle)
{
	//该函数并未对pinCount进行判断？？？？ 怎么处理多线程访问问题呢
	int i,offset;
	for(i=0;i<PF_BUFFER_SIZE;i++){
		if(bf_manager.allocated[i]==false)
			continue;
		if(strcmp(bf_manager.frame[i].fileName,fileHandle->fileName)!=0)
			continue;

		if(bf_manager.frame[i].bDirty==true){
			offset=(bf_manager.frame[i].page.pageNum)*sizeof(Page);
			if(_lseek(fileHandle->fileDesc,offset,SEEK_SET)==offset-1)
				return PF_FILEERR;
			if(_write(fileHandle->fileDesc,&(bf_manager.frame[i].page),sizeof(Page))!=sizeof(Page))
				return PF_FILEERR;
		}
		bf_manager.allocated[i]=false;
	}
	return SUCCESS;
}